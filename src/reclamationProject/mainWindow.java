package reclamationProject;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JList;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import javax.swing.AbstractListModel;
import java.awt.Color;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Font;
import java.awt.Button;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.*;


public class mainWindow {

	public JFrame frame;
	private JTextField username;
	private JTextField password;
	private JLabel lblLogin;
	private JLabel lblPassword;
	private JLabel lblError ;
	public static Connection con;
	public static Statement stt;
	public static int userId;
	public static String userName;
	/**
	 * Create the application.
	 */
	public mainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws ClassNotFoundException 
	 */
	private void initialize() {
		// init connection to mysql database :recla/root/root
		try {
			Class.forName("com.mysql.jdbc.Driver");
			try {
		    this.con  = DriverManager.getConnection("jdbc:mysql://localhost:3306/recla?user=root&password=root");
		    if (con != null) {
		    	 System.out.println("connected");
		    }else {
		    	System.out.println("not connected");
		    }
		    stt = con.createStatement();
		     
		} catch(SQLException e) {
			System.out.println(e);
			}
		
		} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
		 
		// Jfram desgining
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().setForeground(Color.CYAN);
		frame.setBounds(100, 100, 818, 387);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Bonjour sur votre application de reclamation");
		lblNewLabel.setFont(new Font("Te X Gyre Chorus", Font.BOLD | Font.ITALIC, 29));
		lblNewLabel.setBounds(118, 12, 523, 31);
		frame.getContentPane().add(lblNewLabel);
		username = new JTextField();
		username.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
			lblNewLabel.setText(username.getText());

			}
			
		});
		username.setBounds(306, 129, 167, 31);
		frame.getContentPane().add(username);
		username.setColumns(10);
		
		password = new JTextField();
		password.setBounds(306, 196, 167, 31);
		frame.getContentPane().add(password);
		password.setColumns(10);
		
		lblLogin = new JLabel("login");
		lblLogin.setBounds(317, 102, 66, 15);
		frame.getContentPane().add(lblLogin);
		
		lblPassword = new JLabel("password");
		lblPassword.setBounds(317, 169, 85, 15);
		frame.getContentPane().add(lblPassword);
		
		JButton btnConnecte = new JButton("connecte");
		btnConnecte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		
		btnConnecte.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					
				   connecteToSql(username.getText(),password.getText());	
				   
				} catch(SQLException e1) {
					
					e1.getMessage();
				}
			  
			}
		});

		btnConnecte.setBounds(331, 239, 114, 25);
		frame.getContentPane().add(btnConnecte);
		
		JLabel lblNoCompte = new JLabel("no compte ?? ");
		lblNoCompte.setBounds(288, 288, 114, 29);
		frame.getContentPane().add(lblNoCompte);
		
		JLabel lblInscrit = new JLabel("inscrit");
		lblInscrit.setForeground(Color.BLUE);
		lblInscrit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				register kframe = new register("registration");
			    kframe.frame.setVisible(true);
			}
		});
		lblInscrit.setBackground(Color.BLUE);
		lblInscrit.setBounds(442, 295, 66, 15);
		frame.getContentPane().add(lblInscrit);
		lblError = new JLabel("error");
		lblError.setForeground(Color.RED);
		lblError.setVisible(false);
		lblError.setBounds(309, 55, 277, 30);
		frame.getContentPane().add(lblError);
	}
	// function connnect to user
	public  void connecteToSql(String user,String pass)  throws  SQLException {
		
	   
		ResultSet  query = stt.executeQuery("select id,username, password, priv  from users");
		
		
	    while(query.next()) {
	    	if(query.getString(2).equals(user) && query.getString(3).equals( pass)) {
	              userId=Integer.parseInt(query.getString(1));
	              userName=query.getString(2);
	              
	    		 int pri = Integer.parseInt(query.getString(4));
	    		 frame.setVisible(false);
	    		 if(pri == 1) {
	    			 //admin
	    			 Admin_recla admin = new Admin_recla("admin");
	    			             admin.setVisible(true);
	    			 
	    		 }else {
	    			//utilusateur
	    			 User_recla user1 = new User_recla("user");
	    			            user1.frame.setVisible(true);
	    		 }
	    		 
	  
	    		return;
	    		
	    	}
	    		
	    } 
	    
	    lblError.setVisible(true);
	    lblError.setText("incorect username or password ");
	 
	}
}

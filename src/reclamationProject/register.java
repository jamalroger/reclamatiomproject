package reclamationProject;

import java.awt.EventQueue;
import java.sql.*;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Button;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import java.awt.Label;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class register {
	
	public JFrame frame;
	private JTextField textField;
	private JPasswordField textField_1;
	private JPasswordField  textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTextField textField_10;
	private JTextField textField_11;
	/**
	 * Create the application.
	 */
	public register(String app) {
		initialize(app);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(String appName) {
		
		frame = new JFrame(appName);
		frame.getContentPane().setBackground(new Color(255, 255, 255));
		frame.getContentPane().setLayout(null);
		JPanel panel = new JPanel();
		panel.setBackground(Color.DARK_GRAY);
		panel.setBounds(-13, 0, 452, 659);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
//		JLabel label = new JLabel("");
//		label.setHorizontalAlignment(SwingConstants.CENTER);
////		label.setIcon(new ImageIcon(register.class.getResource("/images/Capture_d_e_cran_2015-11-30_a_17_23_48.png")));
//		label.setBounds(-134, -55, 735, 399);
//		panel.add(label);
		
		JLabel lblNewLabel_6 = new JLabel("INSCRIPTION");
		lblNewLabel_6.setForeground(Color.WHITE);
		lblNewLabel_6.setBackground(Color.WHITE);
		lblNewLabel_6.setFont(new Font("Times New Roman", Font.ITALIC, 40));
		lblNewLabel_6.setBounds(124, 374, 245, 83);
		panel.add(lblNewLabel_6);
		
		Button button = new Button("SignUp");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			 
			try {
//				int query = mainWindow.stt.executeUpdate("insert into users(username,password,nom,prenom,email,tel,cin,ddn,adresse,ville,code_postal,priv) values('"
//						+ textField.getText() + "','"
//						+ textField_1.getPassword().toString() + "','"
//						+ textField_3.getText() + "','"
//						+ textField_4.getText() + "','"
//						+ textField_5.getText() + "','"
//						+ textField_6.getText() + "','"
//						+ textField_7.getText() + "','"
//						+ textField_8.getText() + "','"
//						+ textField_9.getText() + "','"
//						+ "'jamal','"
//						+ textField_10.getText() + "',"
//						+ 0 );
				PreparedStatement stt = mainWindow.con.prepareStatement("insert into users("
						+ "username,password,nom,prenom,email,tel,ddn,cin,adresse,ville,code_postal,priv) "
						+ "values(?,?,?,?,?,?,?,?,?,?,?,?)");
				        stt.setString(1, textField.getText());
				        stt.setString(2, String.valueOf(textField_1.getPassword()));
				        stt.setString(3, textField_3.getText());
				        stt.setString(4, textField_4.getText());
				        stt.setString(5, textField_5.getText());
				        stt.setString(6, textField_6.getText());
				        stt.setString(7, textField_7.getText());
				        stt.setString(8, textField_8.getText());
				        stt.setString(9, textField_9.getText());
				        stt.setString(10, textField_10.getText());
				        stt.setString(11, textField_11.getText());
				        stt.setInt(12, 0);
				int query=stt.executeUpdate();
				if(query==1) {
					System.out.print("user created");
					JOptionPane.showMessageDialog(null, "Registratiom reusite");
					mainWindow.userName=textField.getText();
			            frame.setVisible(false);
				} else {
					JOptionPane.showMessageDialog(null, "erreur incorrect donnees");
					System.out.print("incorectt data");
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch blocker
				e.printStackTrace();
			}
			}
		});
		button.setFont(new Font("Dialog", Font.BOLD, 12));
		button.setForeground(Color.WHITE);
		button.setBackground(Color.RED);
		button.setBounds(485, 572, 223, 33);
		frame.getContentPane().add(button);
		
		textField = new JTextField();
		textField.setBounds(488, 38, 220, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Username :");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel.setBounds(488, 24, 100, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("EMAIL :\r\n");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_1.setBounds(488, 243, 46, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		textField_1 = new JPasswordField();
		textField_1.setBounds(488, 82, 223, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password :");
		lblPassword.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPassword.setBounds(488, 69, 100, 14);
		frame.getContentPane().add(lblPassword);
		
		textField_2 = new JPasswordField();
		textField_2.setColumns(10);
		textField_2.setBounds(488, 126, 223, 20);
		frame.getContentPane().add(textField_2);
		
		JLabel lblRepeatpassword = new JLabel("Repeat Password :");
		lblRepeatpassword.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblRepeatpassword.setBounds(488, 113, 111, 14);
		frame.getContentPane().add(lblRepeatpassword);
		
		JLabel lblTel = new JLabel("TEL :");
		lblTel.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTel.setBounds(488, 287, 46, 14);
		frame.getContentPane().add(lblTel);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(488, 169, 223, 20);
		frame.getContentPane().add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(488, 212, 223, 20);
		frame.getContentPane().add(textField_4);
		
		JLabel lblAdresse = new JLabel("CIN :");
		lblAdresse.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAdresse.setBounds(488, 375, 100, 14);
		frame.getContentPane().add(lblAdresse);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(488, 256, 223, 20);
		frame.getContentPane().add(textField_5);
		
		JLabel lblNewLabel_2 = new JLabel("Nom :");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_2.setBounds(488, 157, 46, 14);
		frame.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Pr\u00E9nom :");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_3.setBounds(488, 200, 66, 14);
		frame.getContentPane().add(lblNewLabel_3);
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(488, 301, 223, 20);
		frame.getContentPane().add(textField_6);
		
		JLabel lblNewLabel_4 = new JLabel("Date Naissance format : y-m-d");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_4.setBounds(488, 332, 201, 14);
		frame.getContentPane().add(lblNewLabel_4);
		
		textField_7 = new JTextField();
		textField_7.setColumns(10);
		textField_7.setBounds(488, 344, 223, 20);
		frame.getContentPane().add(textField_7);
		
		textField_8 = new JTextField();
		textField_8.setColumns(10);
		textField_8.setBounds(488, 388, 223, 20);
		frame.getContentPane().add(textField_8);
		
		JLabel lblNewLabel_5 = new JLabel("Adresse :");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_5.setBounds(488, 419, 100, 14);
		
		frame.getContentPane().add(lblNewLabel_5);
		
		textField_9 = new JTextField();
		textField_9.setColumns(10);
		textField_9.setBounds(488, 432, 223, 20);
		frame.getContentPane().add(textField_9);
		
		Button button_1 = new Button("Quitter");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
			}
		});
		button_1.setForeground(Color.WHITE);
		button_1.setFont(new Font("Dialog", Font.BOLD, 12));
		button_1.setBackground(Color.RED);
		button_1.setBounds(485, 626, 223, 33);
		frame.getContentPane().add(button_1);
		
		textField_10 = new JTextField();
		textField_10.setBounds(485, 481, 223, 20);
		frame.getContentPane().add(textField_10);
		textField_10.setColumns(10);
		
		JLabel lblVille = new JLabel("Ville :");
		lblVille.setFont(new Font("Dialog", Font.BOLD, 11));
		lblVille.setBounds(488, 464, 100, 14);
		frame.getContentPane().add(lblVille);
		
		textField_11 = new JTextField();
		textField_11.setColumns(10);
		textField_11.setBounds(488, 535, 223, 20);
		frame.getContentPane().add(textField_11);
		
		JLabel lblCodePostal = new JLabel("Code postal :");
		lblCodePostal.setFont(new Font("Dialog", Font.BOLD, 11));
		lblCodePostal.setBounds(488, 513, 100, 14);
		frame.getContentPane().add(lblCodePostal);
		frame.setBounds(100, 100, 979, 739);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
}
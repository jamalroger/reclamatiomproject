package reclamationProject;

import java.awt.EventQueue;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Button;

import javax.swing.*;

import java.awt.Label;
import java.awt.Toolkit;
import java.awt.Font;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

public class User_recla {

	public JFrame frame;
	private JTextField objet;
	private JTextField content;
	/**
	 * Create the application.
	 */
	public User_recla(String app) {
		initialize(app);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(String app_name) {
		
		frame = new JFrame(app_name);
		frame.setAlwaysOnTop(true);
		frame.setBounds(100, 100, 891, 648);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
//		Dimension DimMax = Toolkit.getDefaultToolkit().getScreenSize();
//		frame.setMaximumSize(DimMax);
//		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(12, 30, 805, 577);
		frame.getContentPane().add(tabbedPane);
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Mon compte", null, panel_1, null);
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_1.setLayout(null);
		
		JLabel lblCompteInformation = new JLabel("Compte Information : ");
		lblCompteInformation.setForeground(Color.BLUE);
		lblCompteInformation.setBounds(121, 40, 176, 23);
		panel_1.add(lblCompteInformation);
		
		JLabel lblUsername = new JLabel("Username :");
		lblUsername.setBounds(88, 102, 170, 15);
		panel_1.add(lblUsername);
		
		JLabel lblNom = new JLabel("Nom :");
		lblNom.setBounds(89, 129, 234, 15);
		panel_1.add(lblNom);
		
		JLabel lblPrenom = new JLabel("Prenom :");
		lblPrenom.setBounds(89, 156, 208, 15);
		panel_1.add(lblPrenom);
		
		JLabel lbldate= new JLabel("Date naissance :");
		lbldate.setBounds(89, 245, 196, 15);
		panel_1.add(lbldate);
		
		JLabel lblCin = new JLabel(" CIN :");
		lblCin.setBounds(89, 272, 131, 15);
		panel_1.add(lblCin);
		
		JLabel lblTelephone = new JLabel("Telephone :");
		lblTelephone.setBounds(89, 218, 196, 15);
		panel_1.add(lblTelephone);
		
		JLabel lblAddresse = new JLabel("addresse :");
		lblAddresse.setBounds(89, 299, 196, 15);
		panel_1.add(lblAddresse);
		
		JLabel lblVille = new JLabel("ville :");
		lblVille.setBounds(89, 326, 184, 15);
		panel_1.add(lblVille);
		
		JLabel lblCodePostal = new JLabel("code postal :");
		lblCodePostal.setBounds(89, 350, 218, 15);
		panel_1.add(lblCodePostal);
		
		JLabel lblEmail = new JLabel("Email :");
		lblEmail.setBounds(89, 193, 218, 15);
		panel_1.add(lblEmail);
		
		JButton btnModifier = new JButton("Modifier");
		btnModifier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnModifier.setBounds(144, 406, 281, 25);
		panel_1.add(btnModifier);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Nouveau reclamation", null, panel, null);
		panel.setLayout(null);
		JLabel lblNewLabel_1 = new JLabel("Nouveau reclamation");
		lblNewLabel_1.setForeground(Color.RED);
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 30));
		lblNewLabel_1.setBackground(Color.WHITE);
		lblNewLabel_1.setBounds(9, 5, 358, 36);
		panel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Type de reclamation :");
		lblNewLabel_2.setFont(new Font("Times New Roman", Font.BOLD, 17));
		lblNewLabel_2.setBounds(9, 117, 210, 20);
		panel.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Object :");
		lblNewLabel_3.setFont(new Font("Times New Roman", Font.BOLD, 17));
		lblNewLabel_3.setBounds(144, 163, 75, 20);
		panel.add(lblNewLabel_3);
		
		objet = new JTextField();
		objet.setBounds(243, 165, 141, 19);
		panel.add(objet);
		objet.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("Description :");
		lblNewLabel_4.setFont(new Font("Times New Roman", Font.BOLD, 17));
		lblNewLabel_4.setBounds(96, 228, 123, 20);
		panel.add(lblNewLabel_4);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setEditable(true);
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Mat\u00E9riel", "Syst\u00E8me", "Service", "r\u00E9ception", ""}));
		comboBox.setBounds(238, 117, 146, 24);
		panel.add(comboBox);
		
		content = new JTextField();
		content.setBounds(243, 230, 343, 100);
		panel.add(content);
		content.setColumns(10);
		
		JButton btnNewButton = new JButton("Envoyer");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			try {
				insertRecla(objet.getText(), content.getText());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			}
		});
		btnNewButton.setForeground(Color.RED);
		btnNewButton.setBackground(Color.BLACK);
		btnNewButton.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		btnNewButton.setBounds(243, 357, 251, 34);
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Quitter");
		btnNewButton_1.setBackground(Color.BLACK);
		btnNewButton_1.setForeground(Color.RED);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});

		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Mais reclamation", null, panel_2, null);
		panel_2.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(59, 97, 188, 303);
		panel_2.add(scrollPane);
		
	
		
		JTextPane contentSelected = new JTextPane();
		contentSelected.setBounds(344, 184, 391, 210);
		panel_2.add(contentSelected);
		
		JLabel lblContent = new JLabel("Content :");
		lblContent.setBounds(344, 155, 66, 17);
		panel_2.add(lblContent);
		
		JLabel lblObject = new JLabel("Object :");
		lblObject.setBounds(344, 97, 58, 17);
		panel_2.add(lblObject);
		DefaultListModel<String> l1 = new DefaultListModel<>();
		JList<String> list = new JList<>(l1);
	    scrollPane.setViewportView(list);
		
		JLabel lblDate = new JLabel("Date :");
		lblDate.setBounds(344, 126, 53, 17);
		panel_2.add(lblDate);
		
		JLabel objetSelected = new JLabel("");
		objetSelected.setBounds(412, 98, 292, 15);
		panel_2.add(objetSelected);
		
		JLabel dateSelected = new JLabel("");
		dateSelected.setBounds(409, 125, 326, 17);
		panel_2.add(dateSelected);		
		
		JLabel lblMaisReclamation = new JLabel("Mais reclamation :");
		lblMaisReclamation.setBounds(59, 70, 188, 15);
		panel_2.add(lblMaisReclamation);
		//get Selected Reclamation
        list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				ResultSet query;
				try {
					query = mainWindow.stt.executeQuery("select * from reclamation where objet='" + list.getSelectedValue()+"' and cli_id="+mainWindow.userId);
					while(query.next()) {
					objetSelected.setText(query.getString(3));
					dateSelected.setText(query.getString(4));
					contentSelected.setText(query.getString(5));
				  }
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				  

				
			}
		});
        
        // add all reclamation objet to list
        
			 try {
			 
			 
			 
				ResultSet query = mainWindow.stt.executeQuery("select objet from reclamation where cli_id=" + mainWindow.userId );
				  while(query.next()) {
					  l1.addElement(query.getString(1));
				  }
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
		list.setSelectedIndex(0);
		
		JPanel panel_3 = new JPanel();
		tabbedPane.addTab("Afficher l'aide", null, panel_3, null);

// inisialise panel Mon compte	
		
 try {
		ResultSet query = mainWindow.stt.executeQuery("select * from users where id=" + mainWindow.userId );
		  while(query.next()) {
			  lblUsername.setText(lblUsername.getText()+ " " + query.getString(2));
			  lblNom.setText(lblNom.getText()+ " " + query.getString(4));
			  lblPrenom.setText(lblPrenom.getText()+ " " + query.getString(5));
			  lblEmail.setText(lblEmail.getText()+ " " + query.getString(6));
			  lblTelephone.setText(lblTelephone.getText()+ " " + query.getString(7));
			  lbldate.setText(lbldate.getText()+ " " + query.getString(8));
			  lblCin.setText(lblCin.getText()+ " " + query.getString(9));
			  lblAddresse.setText(lblAddresse.getText()+ " " + query.getString(10));
			  lblVille.setText(lblVille.getText()+ " " + query.getString(11));
			  lblCodePostal.setText(lblCodePostal.getText()+ " " + query.getString(12));

		  }
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	
}
// insert new reclamation
public void insertRecla(String objet,String content) throws SQLException {
		  DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
		  LocalDateTime now = LocalDateTime.now();  
		PreparedStatement stt = mainWindow.con.prepareStatement("insert into reclamation("
				+ "cli_id,objet,Date,contenu) "
				+ "values(?,?,?,?)");
		        stt.setInt(1,mainWindow.userId );
		        stt.setString(2, objet);
		        stt.setString(3,dtf.format(now));
		        stt.setString(4,content);
		        
		int query=stt.executeUpdate();
		if(query==1) {
			System.out.print("reclamation insert");	
			JOptionPane.showMessageDialog(null, "Registration envoyer merci !!");
		} else {
			System.out.print("incorectt data");
		}
		
		
	}
}
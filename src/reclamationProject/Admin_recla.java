package reclamationProject;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.ListModel;
import javax.swing.JTextPane;

public class Admin_recla extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public Admin_recla(String name) {
		super(name);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 849, 470);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(5, 61, 808, 366);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Liste Utilisateur", null, panel, null);
		panel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(41, 49, 191, 278);
		panel.add(scrollPane);
		 DefaultListModel<String> l1 = new DefaultListModel<>();
						  
		DefaultListModel<String> l2 = new DefaultListModel<>();
		JList<String> list = new JList<>(l1);
		
		scrollPane.setViewportView(list);
		scrollPane.setBounds(41, 49, 191, 278);
		
			
			JLabel lblCompteInformation = new JLabel("Compte Information : ");
			lblCompteInformation.setForeground(Color.BLUE);
			lblCompteInformation.setBounds(224, 19, 176, 23);
			panel.add(lblCompteInformation);
			
			JLabel lblUsername = new JLabel("Username :");
			lblUsername.setBounds(326, 42, 87, 15);
			panel.add(lblUsername);
			
			JLabel lblNom = new JLabel("Nom :");
			lblNom.setBounds(326, 70, 87, 15);
			panel.add(lblNom);
			
			JLabel lblPrenom = new JLabel("Prenom :");
			lblPrenom.setBounds(326, 97, 74, 15);
			panel.add(lblPrenom);
			
			JLabel lbldate= new JLabel("Date naissance :");
			lbldate.setBounds(326, 259, 117, 15);
			panel.add(lbldate);
			
			JLabel lblCin = new JLabel(" CIN :");
			lblCin.setBounds(326, 205, 76, 15);
			panel.add(lblCin);
			
			JLabel lblTelephone = new JLabel("Telephone :");
			lblTelephone.setBounds(326, 151, 87, 15);
			panel.add(lblTelephone);
			
			JLabel lblAddresse = new JLabel("adresse :");
			lblAddresse.setBounds(326, 232, 98, 15);
			panel.add(lblAddresse);
			
			JLabel lblVille = new JLabel("ville :");
			lblVille.setBounds(326, 178, 76, 15);
			panel.add(lblVille);
			
			JLabel lblCodePostal = new JLabel("code postal :");
			lblCodePostal.setBounds(326, 283, 112, 23);
			panel.add(lblCodePostal);
			
			JLabel lblEmail = new JLabel("Email :");
			lblEmail.setBounds(326, 124, 87, 15);
			panel.add(lblEmail);
			
				
				JLabel Username = new JLabel("New label");
				Username.setBounds(426, 42, 66, 15);
				panel.add(Username);
				
				JLabel Nom = new JLabel("New label");
				Nom.setBounds(425, 69, 66, 15);
				panel.add(Nom);
				
				JLabel Prenom = new JLabel("New label");
				Prenom.setBounds(430, 97, 66, 15);
				panel.add(Prenom);
				
				JLabel Email = new JLabel("New label");
				Email.setBounds(425, 124, 66, 15);
				panel.add(Email);
				
				JLabel Telephone = new JLabel("New label");
				Telephone.setBounds(430, 151, 66, 15);
				panel.add(Telephone);
				
				JLabel Ville = new JLabel("New label");
				Ville.setBounds(430, 178, 66, 15);
				panel.add(Ville);
				
				JLabel Cin = new JLabel("New label");
				Cin.setBounds(430, 205, 66, 15);
				panel.add(Cin);
				
				JLabel Adresse = new JLabel("New label");
				Adresse.setBounds(436, 232, 66, 15);
				panel.add(Adresse);
				
				JLabel Naissance = new JLabel("ddddddddddd");
				Naissance.setBounds(459, 259, 158, 15);
				panel.add(Naissance);
				
				JLabel Codepostale = new JLabel("New label");
				Codepostale.setBounds(450, 287, 66, 15);
				panel.add(Codepostale);
				list.setSelectedIndex(0);
				//get information about users selected in the list	
				list.addListSelectionListener(new ListSelectionListener() {
						public void valueChanged(ListSelectionEvent arg0) {
							
							try {
								ResultSet query = mainWindow.stt.executeQuery("select * from users where id=" + list.getSelectedIndex() );
								  while(query.next()) {
									  Username.setText(query.getString(2));
									  Nom.setText(query.getString(4));
									  Prenom.setText(query.getString(5));
									  Email.setText( query.getString(6));
									  Telephone.setText(query.getString(7));
									  Naissance.setText(query.getString(8));
									  Cin.setText(query.getString(9));
									  Adresse.setText( query.getString(10));
									  Ville.setText(query.getString(11));
									  Codepostale.setText(query.getString(12));
								  }
								
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
				
						}
					});

		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Options", null, panel_2, null);
		
		JPanel panel_3 = new JPanel();
		tabbedPane.addTab("L'aide", null, panel_3, null);
		
	     JScrollPane scrollPane1 = new JScrollPane();
	     scrollPane1.setBounds(72, 54, 225, 273);
	     
	     JPanel panel_1 = new JPanel();
	     tabbedPane.addTab("Liste reclamtion", null, panel_1, null);
	     panel_1.setLayout(null);
	     panel_1.add(scrollPane1);
	     
	     JList<String> list_1 = new JList<String>(l2);
	   	     scrollPane1.setViewportView(list_1);
	     
	     JLabel lblCreerPar = new JLabel("Creer par :");
	     lblCreerPar.setBounds(390, 41, 100, 30);
	     panel_1.add(lblCreerPar);
	     
	     JLabel UserNom = new JLabel("New label");
	     UserNom.setBounds(490, 41, 131, 30);
	     panel_1.add(UserNom);
	     
	     JLabel lblDateDeCreation = new JLabel("Date de Creation");
	     lblDateDeCreation.setBounds(390, 71, 131, 30);
	     panel_1.add(lblDateDeCreation);
	     
	     JLabel UserDate = new JLabel("");
	     UserDate.setBounds(527, 71, 131, 15);
	     panel_1.add(UserDate);
	     
	     JLabel lblDansLobjet = new JLabel("Dans l'objet");
	     lblDansLobjet.setBounds(390, 97, 121, 30);
	     panel_1.add(lblDansLobjet);
	     
	     JLabel UserObjet = new JLabel("New label");
	     UserObjet.setBounds(512, 105, 146, 15);
	     panel_1.add(UserObjet);
	     
	     JTextPane UserContent = new JTextPane();
	     UserContent.setBounds(390, 170, 258, 133);
	     panel_1.add(UserContent);
	     
	     JLabel lblContent = new JLabel("Content :");
	     lblContent.setBounds(390, 139, 100, 19);
	     panel_1.add(lblContent);
	     list_1.addListSelectionListener(new ListSelectionListener() {
		     	public void valueChanged(ListSelectionEvent arg0) {
		     	try {
		    			ResultSet query = mainWindow.stt.executeQuery("select username,nom,prenom,Date,objet,contenu from users,reclamation where users.id=reclamation.cli_id and reclamation.id = " + list_1.getSelectedIndex());
		    			  while(query.next()) { 
		                     UserNom.setText(query.getString(2) +" "+ query.getString(3));
		                     UserDate.setText(query.getString(4));
		                     UserObjet.setText(query.getString(5));
		                     UserContent.setText(query.getString(6));
		    			  }
		    			  
		    			
		    		} catch (SQLException e) {
		    			// TODO Auto-generated catch block
		    			e.printStackTrace();
		    		}
		    	
		     		
		     	}
		     });
  

	//get liste Users and set to list
			try {
			ResultSet query = mainWindow.stt.executeQuery("select username from  users" );
			  while(query.next()) {
				  l1.addElement(query.getString(1));
			  }
		  query = mainWindow.stt.executeQuery("select objet from  reclamation" );
			  while(query.next()) {
				  l2.addElement(query.getString(1));
			  }
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
}
